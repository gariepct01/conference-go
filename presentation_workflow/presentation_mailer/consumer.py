import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_message(ch, method, properties, body):
            print("  Received %r" % body)

        # Probably use json.load on the body to get data
        def presentation_approvals(ch, method, properties, body):
            message = json.loads(body)
            email_body = (
                message["presenter_name"]
                + ", we're happy to tell you that your presentation "
                + message["title"]
                + " has been accepted!"
            )
            send_mail(
                "Your presentation has been accepted!",
                email_body,
                "admin@conference.go",
                [message["presenter_email"]],
                fail_silently=False,
            )

        def process_approval(channel):
            channel.queue_declare(queue="presentation_approvals")
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=presentation_approvals,
                auto_ack=True,
            )

        def presentation_rejections(ch, method, properties, body):
            message = json.loads(body)
            email_body = (
                message["presenter_name"]
                + ", we're sorry to tell you that your presentation "
                + message["title"]
                + " has been rejected."
            )
            send_mail(
                "Your presentation has been rejected",
                email_body,
                "admin@conference.go",
                [message["presenter_email"]],
                fail_silently=False,
            )

        def process_rejection(channel):
            channel.queue_declare(queue="presentation_rejections")
            channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=presentation_rejections,
                auto_ack=True,
            )

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            process_approval(channel)
            process_rejection(channel)
            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
