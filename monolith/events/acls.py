from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


# Uses the pexel API to retrieve a photo, if available
def get_picture(city, state):
    # Create a dictionary for the headers to use in the request
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    URL = "https://api.pexels.com/v1/search"
    # Make the request
    response = requests.get(
        URL,
        params=query,
        headers=headers,
    )
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        picture_url = content["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except (KeyError, IndexError):
        return {"picture_url": None}


# Uses the open weather API to get the weather, if available
def get_weather(city, state):
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, {state}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }

    response = requests.get(weather_url, params=weather_params)
    content = json.loads(response.content)

    info = {
        "temperature": content["main"]["temp"],
        "description": content["weather"][0]["description"],
    }
    return info
