# Generated by Django 4.0.3 on 2023-05-11 18:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_conference_picture_url'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='picture_url',
        ),
        migrations.AddField(
            model_name='location',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
